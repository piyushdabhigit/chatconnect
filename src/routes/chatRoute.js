// routes/chatRoute.js
const express = require('express');
const router = express.Router();
const Chat = require('../models/chat.js');
const User = require('../models/user.js');

module.exports = function(io) {
  // Helper function to get a random user ID
  const getRandomUserId = async () => {
    try {
      const users = await User.find();
      if (users.length === 0) {
        const newUser = new User({
          username: generateRandomUsername()
        });
        await newUser.save();
        return newUser._id.toString(); // Return as string
      } else {
        const randomIndex = Math.floor(Math.random() * users.length);
        return users[randomIndex]._id.toString(); // Return as string
      }
    } catch (err) {
      throw new Error('Error getting random user ID: ' + err.message);
    }
  }; 
  // Function to generate a random username (optional)
  const generateRandomUsername = () => {
    const randomSuffix = Math.floor(Math.random() * 1000);
    return `user_${randomSuffix}`;
  };

  // Endpoint for connecting users to a random chat
  router.post('/connect', async (req, res) => {
    try {
      console.log('abc');
      // Get two random user IDs
      const userId1 = await getRandomUserId();
      const userId2 = await getRandomUserId();
  
      // Create a new chat with the two users
      const chat = new Chat({
        participants: [userId1, userId2],
        messages: [],
      });
      await chat.save();
  
      // Emit chatConnected event to clients via Socket.IO
      io.emit('chatConnected', { chatId: chat._id });
  
      res.status(200).json({ message: 'Connected to a random chat', chatId: chat._id });
    } catch (err) {
      res.status(500).json({ error: err.message });
    }
  }); 
  // Endpoint for sending a message in a chat
  router.post('/:chatId/send', async (req, res) => {
    try {
      const { chatId } = req.params;
      const { message } = req.body;
  
      // Find the chat by its ID
      const chat = await Chat.findById(chatId);
  
      if (!chat) {
        return res.status(404).json({ error: 'Chat not found' });
      }
  
      // Add the message to the chat
      chat.messages.push(message);
      await chat.save();
  
      // Emit messageSent event to clients via Socket.IO
      io.emit('messageSent', { chatId: chat._id, message });
  
      res.status(200).json({ message: 'Message sent successfully' });
    } catch (err) {
      res.status(500).json({ error: err.message });
    }
  }); 
  // Endpoint for fetching chat history
  router.get('/:chatId/history', async (req, res) => {
    try {
      const { chatId } = req.params;

      // Find the chat by its ID
      const chat = await Chat.findById(chatId);

      if (!chat) {
        return res.status(404).json({ error: 'Chat not found' });
      }

      // Return chat history
      res.status(200).json({ history: chat.messages });
    } catch (err) {
      res.status(500).json({ error: err.message });
    }
  }); 
  return router;
};